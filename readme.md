## About
This is a simple, fun landing page which are you are using the ingrediants below and put together

[![Build Status](https://travis-ci.org/Dogfalo/materialize.svg?branch=master)](https://travis-ci.org/Dogfalo/materialize)
[![Build Status](https://travis-ci.org/twbs/bootstrap.svg?branch=master)](https://travis-ci.org/twbs/bootstrap)
[![License](https://poser.pugx.org/clnt/materialize/license)](https://packagist.org/packages/clnt/materialize)


### Official Material Documentation
[![Total Downloads](https://poser.pugx.org/clnt/materialize/downloads)](https://packagist.org/packages/clnt/materialize) [![Build Status](https://travis-ci.org/Dogfalo/materialize.svg?branch=master)](https://travis-ci.org/Dogfalo/materialize)

Created and designed by Google, Material Design is a design language that combines the classic principles of successful design along with innovation and technology. Google's goal is to develop a system of design that allows for a unified user experience across all their products on any platform.
[Currently v0.97.6-alpha](http://materializecss.com/).


### Official Bootstrap Documentation
![Total Downloads](https://poser.pugx.org/twbs/bootstrap/d/total.svg) [![Build Status](https://travis-ci.org/twbs/bootstrap.svg?branch=master)](https://travis-ci.org/twbs/bootstrap)

Bootstrap is the world’s most popular framework for building responsive, mobile-first sites and applications. Inside you’ll find high quality HTML, CSS, and JavaScript to make starting any project easier than ever. [Currently v4.0.0-alpha.2](http://v4-alpha.getbootstrap.com).


### License
Portfolio is powered by Bagels, ❤ and LCO(LetsCode.Online) which are is a GNU software licensed under the [MIT](https://opensource.org/licenses/MIT)

![alt tag](https://raw.github.com/dogfalo/materialize/master/images/materialize.gif)